#!/usr/bin/env python

import socket

with open('hosts.txt') as f:
    HOSTS = [line.strip() for line in f.readlines()]
    HOSTS = set(HOSTS)

def host_ip(host):
    try:
        ip = socket.gethostbyname(host)
    except socket.gaierror as e:
        return None
    return ip

def get_ips(domain):
    for host in HOSTS:
        host2 = '%s.%s' % (host, domain)
        ip = host_ip(host2)
        if ip is None:
            continue
        else:
            yield host2, ip

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('domain')
    parser.add_argument('-o', '--out', default=None)
    args = parser.parse_args()
    if args.out is None:
        args.out = args.domain + '.txt'
    with open(args.out, 'w') as f:
        f.write('%s,%s\n' % (args.domain, host_ip(args.domain)))
    for host, ip in get_ips(args.domain):
        print('%s: %s' % (host, ip))
        with open(args.out, 'a') as f:
            f.write('%s,%s\n' % (host, ip))
    print('Done.')
    
